# FILE:         calient_mount.sh
# AUTHOR:       Sang Han
# VERSION:      1.0.0
# COMPANY:      Calient Technologies
# DESCRIPTION:  Shell library for automating mounting SMB shares on calient networks.

# TODO: Rely less on global variables by implementing
#       User IO
#       Smart Control Structures

# Global Variables
AUTOPROBER="autoprober:autoprober"

# Unit Tests
test_names() {
    echo "\$SHARE is ${SHARE}"
    echo "\$NETBIOS is ${NETBIOS}"
    echo "\$CREDENTIALS is ${CREDENTIALS}"
    echo "\$BASEDIR is ${BASEDIR}"
    echo ""
}

check_os() {
    # Checks uname and outputs $BASEDIR and $OPERATING_SYSTEM variables for use
    local OPERATING_SYSTEM=$(uname -s)
    if [[ "$OPERATING_SYSTEM" =~ "Darwin" ]]; then
        BASEDIR="Volumes"
    elif [[ "$OPERATING_SYSTEM" =~ "Linux" ]]; then
        BASEDIR="mnt"
    elif [[ "$OPERATING_SYSTEM" =~ "CYGWIN" ]]; then
        BASEDIR="mnt"
    else
        printf "Your operating system is not supported\n" >&2
        exit 1
    fi
}

check_mount() {
    local MOUNT_PATH=$(mount | grep "${SHARE/ /%20}" | cut -d ' ' -f3-)
    if [[ \
        "${MOUNT_PATH/(*)/}" =~ "/${BASEDIR}/${SHARE}" \
        ]]; then
        printf "%-15s already mounted at %s\n" "$(tput setaf 1)${SHARE}$(tput sgr0)" "${MOUNT_PATH/(*)/}" >&2
        exit 1
    else
        return
    fi
}

mount_drive() {
    local MOUNT_PATH="/${BASEDIR}/${SHARE}"
    printf "Mounting drive %16s at %s\n" "${SHARE}" "${MOUNT_PATH}"
    mkdir "${MOUNT_PATH}" >/dev/null
    mount_smbfs //"${CREDENTIALS}"@"${NETBIOS}"/"${SHARE/' '/%20}" \
        "${MOUNT_PATH}"
}

unmount_drive() {
    local MOUNT_PATH="/${BASEDIR}/${SHARE}"
    printf "Drive %15s already exists, unmounting\n" ""$(tput setaf 1)"$MOUNT_PATH"$(tput setaf 0)""
    { rmdir $MOUNT_PATH || umount "$MOUNT_PATH"; } 2>/dev/null
}

mount_share() {
    local NETBIOS=${1}
    local SHARE=${2}
    local CREDENTIALS=${3}
    check_os
    check_mount
    mount_drive
}
