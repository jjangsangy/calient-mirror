# FILE:         colors.sh
# AUTHOR:       Sang Han
# VERSION:      1.0.0
# DESCRIPTION:  Library functions used for applying colors to shell
#               scripts.

color_printout() {
    # Takes 2 arguments
    # Argument #1 specifies output quantity displayed on screen
    # Argument #2 for text to be displayed
    # Default output will display "Hello World" in the first 10 colors
    # of tput
    local output="${1:-"10"}"
    local string="${2:-"Hello World"}"
    for (( i=0; i<${output}; i++ )); do
        printf "%s: tput setaf %s\n" \
            "$(tput setaf ${i})${string}" \
            "${i}$(tput sgr0)"
    done
}

color_general() {
    local string=$(tput setaf ${2:-"1"})"${1}"$(tput sgr0)
    printf "${string}"
}

color_red() {
    local string=$(tput setaf 1)"${1}"$(tput sgr0)
    printf "${string}"
}

color_green() {
    local string="$(tput setaf 2)${1}$(tput sgr0)"
    printf "${string}"
}

color_yellow() {
    local string="$(tput setaf 3)${1}$(tput sgr0)"
    printf "${string}"
}

color_blue() {
    local string="$(tput setaf 4)${1}$(tput sgr0)"
    printf "${string}"
}
