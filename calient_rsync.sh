#!/bin/bash
#
# FILE:         Calient Log Sync Tool
# AUTHOR:       Sang Han
# VERSION:      1.1.0
# COMPANY:      Calient Technologies
# DESCRIPTION:  This tool is used to locally sync important log files for quick parsing.
#               Currently only compatable with Mac OS X based machines
# DEPENDS:      mount_calient.sh

# Global Variables
PROGRAM_NAME=$(basename "$0")
PROGRAM_DIRECTORY=$(dirname "${BASH_SOURCE[0]}")
LOGDIR="${PROGRAM_DIRECTORY}/logs"
FLAGS="-avzr --delete-excluded --progress"

if [[ $OSTYPE =~ "darwin" ]]; then
    MOUNT_DIRECTORY="/Volumes"
    DESTINATION="${HOME}/Desktop/Work"
elif [[ $OSTYPE =~ "linux" ]]; then
    MOUNT_DIRECTORY="/mnt"
    DESTINATION="${HOME}"
else
    printf "Your $OSTYPE OSTYPE is currently not supported" >&2
    exit 1
fi

# Source Library
source "${PROGRAM_DIRECTORY}"/lib/calient_mount.sh

usage() {
    cat <<- EOF
    $PROGNAME [-h] [-t]

    This tool is used to locally sync important log files for quick parsing.
    Currently only compatable with Mac OS X based machines

    AUTHOR:     Sang Han
    COMPANY:    Calient Technologies
    YEAR:       2013
    VERSION:    1.1.0

    -h [help]
        Outputs usage directions
    -t [test]
        Runs unit tests

EOF
    exit 0
}

# Check Share Mounting
# TODO: Move feature over to mount_calient library
drive_check() {
    if ! [[ -d /Volumes/Public ]] || ! [[ -d /Volumes/PubStore ]] || ! [[ -d /Volumes/Optical\ Comp ]]; then
        . "$(dirname "${BASH_SOURCE[0]}")/lib/calient_mount.sh"

        if ! [[ -d /Volumes/Public ]]; then
            mount_share columba Public ${AUTOPROBER} || \
                rmdir /Volumes/Public
        fi

        if ! [[ -d /Volumes/PubStore ]]; then
            mount_share sbnetapp PubStore ${AUTOPROBER} || \
                rmdir /Volumes/PubStore
        fi

        if ! [[ -d /Volumes/Optical\ Comp ]]; then
            mount_share columba Optical\ Comp ${AUTOPROBER} || \
                rmdir /Volumes/Optical\ Comp
        fi

    fi
}

# Unit Tests
test_flags () {
    echo "\${FLAGS} is ${FLAGS}"
    echo "\${rsync_source} is ${rsync_source}"
    echo "\${rsync_dest} is ${rsync_dest}"
    return
}


rsync_files() {
    # In development
    while getopts "s:d:" OPTION; do
        case ${OPTION} in
            s) RSYNC_SOURCE="${OPTARG}"
                ;;
            d) RSYNC_DEST="${OPTARG}"
                ;;
            \?)
                echo "Invalid option: -${OPTARG}" >&2
                exit 1
                ;;
        esac
    done
        shift $(($OPTIND-1))

}

rsync_pin() {
    local rsync_source="${MOUNT_DIRECTORY}/PubStore/_Production Data/MEMs PROBE DATA/OSF"
    local rsync_dest="${DESTINATION}/Pin Logs"

    # Check for directory
    if ! [[ -d $rsync_dest ]]; then
        printf "No,directory exists at %s\nDirectory being made\n" "$rsync_dest"
        mkdir "$rsync_dest"
    fi

    if ! [[ -d $LOGDIR ]]; then
        printf "No,directory exists at %s\nDirectory being made\n" "$LOGDIR"
        mkdir $LOGDIR
    fi

    touch "${LOGDIR}/pin.log" && date "+[%m-%d-%Y %Z] [%r]" | tee -a "${LOGDIR}/pin.log"
    rsync ${FLAGS} \
        --exclude='*jpg' \
        --exclude='*ppt' \
        --exclude='*txt' \
        --exclude='*doc' \
        --exclude='*mat' \
        --exclude='*html' \
        --exclude='*dat' \
        --exclude='*db' \
        --exclude='*DS_Store' \
            "${rsync_source}" "${rsync_dest}" | \
                (awk -F "/" \
                '/PST/ {print} /[A-Z][0-9][0-9][0-9]-[0-9][0-9] R[0-9]C[0-9]/{printf("%.12s\n",$3)}'; printf "\n") | \
                    tee -a "${LOGDIR}/pin.log"
}

rsync_mems() {
    local rsync_source="/Volumes/Optical Comp/Inventory/MEMS/"
    local rsync_dest="$HOME/Desktop/Work/MEMS/"
    touch "${LOGDIR}/mems.log" && date "+[%m-%d-%Y %Z] [%r]" | tee -a "${LOGDIR}/mems.log"
    rsync ${FLAGS} \
        --exclude='*lnk' \
        --exclude='*pptx' \
        --exclude='*JPG' \
        --exclude='*db' \
        --exclude='\~\$*' \
            "${rsync_source}" "${rsync_dest}" | grep -E 'PST|Probing'
    return
}

rsync_q() {
    local rsync_source="/Volumes/Public/Calient/AutoProber/Data/Partial Probe/B010"
    local rsync_dest="$HOME/Desktop/Work/Q Data/"
    rsync  ${FLAGS} \
        --exclude='*html' \
        --exclude='*ext' \
        --exclude='*jpg' \
        --exclude='*db' \
            "${rsync_source}" "${rsync_dest}"
    return
}

cleanup_files() {
    # Method to clean log files of unnecessary output from rsync
    # TODO: Unfinished method, needs rewrite making sure to trap signals
    sed -i .bak /'sending incremental file list'/d "${LOGDIR}/${1}.log"
    return
}

# Parse Options
declare -i TEST=0
while getopts ":ht" OPTION; do
    case ${OPTION} in
        h|help)
            usage
            ;;
        t|test)
            TEST=1
            ;;
        \?)
            echo "Invalid option: -${OPTARG}" >&2
            exit 1
            ;;
    esac
done
    shift $(($OPTIND-1))

if [[ "$0" == "${BASH_SOURCE}" ]]; then
    rsync_pin
#   trap cleanup_files EXIT
fi
