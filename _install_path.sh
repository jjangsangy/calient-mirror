#!/bin/bash
#
# FILE:         Install
# AUTHOR:       Sang Han
# VERSION:      0.0.0
# DESCRIPTION:  Installs path specifications for using dotfiles
# DEPENDS:      bash
#
usage() {
    cat <<- DOCUMENT
        $(basename $0) [-f filename] [-d directory] [-h help] [ -t test ]

        Automated script for installing the correct PATH enviornmental variable
        into home directory .bash_profile

        -f [filename]
            Print out filename of install script
        -d [directory]
            Print out directory of install script
        -h [help]
            Displays usage information
        -t [test]
            Test mode. Run all unit tests and logging

        AUTHOR:       Sang Han
        COMPANY:      Calient Technologies

DOCUMENT
        exit 0
}

# Find location of binaries
SCRIPTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd && echo x)"
SCRIPTDIR="${SCRIPTDIR%x}"

# Options
TEST=false
while getopts ":fdht" options; do
    case ${options} in
        f) echo $(BASENAME $0) >&2
           exit 1
           ;;
        d) echo $(BASENAME) >&2
           exit 1
           ;;
   h|help) usage >&2
           exit 1
           ;;
   t|test) TEST=true
           ;;
       \?) echo "Invalid options -${OPTARG}" >&2
           exit 1
           ;;
    esac
done

# Unit Tests
_test_globs() {
    # Scope checking that global variables in case of mutation
    echo "SCRIPTDIR is set to ${SCRIPTDIR}"
    echo "DIRECTORY is set to ${DIRECTORY}"
    echo "TEST is set to ${TEST}"
    return
}

main() {
    if [[ -e $HOME/.bash_profile ]]; then
        echo "Exporting Directory into bash_profile"
        echo -n 'export PATH=$PATH:'${SCRIPTDIR} >> "$HOME/.bash_profile"
    else
        echo "No bash_profile found at $HOME/.bash_profile"
        echo "One will be created for you"
        touch $HOME/.bash_profile
        main
    fi
    return
}

if [[ $TEST=true ]]; then _test_globs; fi

if [[ $FILENAME ]]; then
    usage
    main
fi

if [[ "$0" == "${BASH_SOURCE}" ]]; then

write_path() {
    local path_dir="$1"
    local rc_doc
        cat <<- WRITE >> "${HOME}/${rc_doc}"

        if [[ -d \$]]; then
            . \$HOME/${path_dir}
        fi

WRITE
    }

    check_bin() {
        if ! [[ -d $HOME/bin ]]; then
            echo "User bin directory does not exist"
            echo "Creating bin directory"
            mkdir $HOME/bin
        fi

        if ! [[ $PATH =~ $HOME/bin ]]; then
            echo "Adding bin directory to \$PATH"
            write_path bin .profile
        fi
    }

    main
fi
