#!/bin/bash
#
# VERSION: 0.0.0
# Simple awk script parses for Q tests run by user run at particular dates
#
LOCALQ="$HOME/Desktop/Work/Q Data/B010"
OPERATOR=${1}
FILEDATE=${2}

_test_globs () {
    echo "\${1} is ${1}"
    echo "\${2} is ${2}"
    echo "\${LOCALQ} is ${LOCALQ}"
    echo "\${OPERATOR} is ${OPERATOR}"
    echo "\${FILEDATE} is ${FILEDATE}"
}

main () {
    find "${LOCALQ}" ! -name '*hj*' -name '*Summary.txt' | parallel echo {/.} | grep -e "${OPERATOR}"
}

main
