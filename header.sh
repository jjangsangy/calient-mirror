#!/bin/bash
#
# FILE:         $NAME
# AUTHOR:       Sang Han
# VERSION:      0.0.0
# COMPANY:      Calient Technologies
# DESCRIPTION:  $DOCUMENT
# DEPENDS:      $DEPENDENCIES

usage() {
    cat <<- DOCUMENT

    $(basename $0) [-h help]

    # DESCRIPTION:  $DOCUMENT

    AUTHOR:     Sang Han
    COMPANY:    Calient Technologies
    YEAR:       $DATE %y
    VERSION:    0.0.0

    -h [help]
        Outputs usage directions

DOCUMENT
    exit 0
}

# Source Library
. "$(dirname "${BASH_SOURCE[0]}")/lib/$SOURCE"

# Options
while getopts ":h" options; do
    case ${options} in
        h|help)
            usage
            ;;
        \?)
            echo "Invalid option: -${OPTARG}" >&2
            exit 1
            ;;
    esac
done

main() {
    return
}

if [[ "$0" == "${BASH_SOURCE}" ]]; then
    main
fi
