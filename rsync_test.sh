#!/bin/bash

exclude_files() {
    for (( i=0; i<${#EXCLUDES[@]}; i++ )); do
        printf "%sexclude=*%s " "--" "${EXCLUDES[i]}"
    done
}

rsync_files() {
    FLAGS="-avzr --delete-excluded --progress"
    while getopts ":s:d:" OPTION; do
        case ${OPTION} in
            s) RSYNC_SOURCE="${OPTARG}"
                ;;
            d) RSYNC_DEST="${OPTARG}"
                ;;
            \?)
                echo "Invalid option: -${OPTARG}" >&2
                exit 1
                ;;
        esac
    done
        shift $(($OPTIND-1))

    declare -i PARAM=$# COUNT=1
    declare -a EXCLUDES
    while (($PARAM>0)); do
        EXCLUDES+=("${!COUNT}")
        ((PARAM--))
        shift
    done

    EXCLUDE_LIST="$(exclude_files)"
    rsync ${FLAGS} ${EXCLUDE_LIST} "${RSYNC_SOURCE}" "${RSYNC_DEST}"
}

rsync_files -d "$HOME/Desktop/Work/Pin Logs" -s "/Volumes/PubStore/_Production Data/MEMs PROBE DATA/OSF" jpg ppt txt doc mat html dat db DS_Store


