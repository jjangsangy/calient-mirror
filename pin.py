#!/usr/bin/env python
#
# FILE:         pin
# AUTHOR:       Sang Han
# VERSION:      0.1
# COMPANY:      Calient Technologies
# DESCRIPTION:  Pin Prober Parsing Script
# DEPENDS:      calient_mount.sh library

import sys

binning=(
        '101 - Shorted',
        '102 - Over Current',
        '103 - No Deflection',
        '104 - Bad Initial Angle',
        '106 - V Limit Poor Deflect',
        '107 - M Limit Poor Deflect',
        '108 - V Limit Full Deflect',
        '109 - M Limit Full Deflect',
        '100 - Full Deflection'
        )

def count_args():
    # If more than 2 arguments are passed by shell, exit with error code 1
    if len(sys.argv) - 1 > 2:
        sys.stderr.write('Too many arguments\n')

def main():
    count_args()

if __name__ == '__main__':
    main()
